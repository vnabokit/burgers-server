# Backend of training project "Burgers"

The training project "Burgers" demonstrates simple usage of CRUD operations.
It is assumed this backend in used in coupling with the respective frontend. 

## Stack

* Node.js
* Express
* PostgreSQL
* db-migrate

# App demo

The app demo (frontend + backend) is available:  <https://burgers-client.herokuapp.com>

# Frontend source code

<https://gitlab.com/vnabokit/burgers-client.git>


# How to deploy

1. Download this repo
2. Set you own database credentials into the ```.env``` file
3. Launch ```npm install```
4. Launch ```npm start```  
5. Start the frontend.