-- Table: public.burgers

CREATE TABLE IF NOT EXISTS public.burgers
(
    id serial PRIMARY KEY,
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    price numeric,
    description text COLLATE pg_catalog."default",
    img_src text COLLATE pg_catalog."default",
    date_added timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

insert into public.burgers (id, title, price, description, img_src) values (
    '1',
	'Classic Cast Iron Smash Burger',
	'9.99',
	'This cheese burger is the culmination of several simple steps that, when executed properly, create one of the most amazing meals that you could ever put in your mouth. [<a href="https://www.bigoven.com/recipe/classic-cast-iron-smash-burger/2276590">src</a>]',
	'https://bigoven-res.cloudinary.com/image/upload/h_320,w_320,c_fill/classic-cast-iron-smash-burger-1ea636.jpg'
) ON CONFLICT (id) DO NOTHING;

insert into public.burgers (id, title, price, description, img_src) values (
    '2',
	'Smile Burger',
	'8.77',
	'Smile Burger is a chain of burger joints in Kamurocho and Downtown Ryukyu. An inaccessible third outlet exists on Showa Street across from Sushi Gin. Yakuza Kiwami 2 adds in another Smile Burger on Northwest Theater Square, as well as one on East Shofukucho Street in Sotenbori. [<a href="https://yakuza.fandom.com/wiki/Smile_Burger">src</a>]',
	'https://static.wikia.nocookie.net/yakuza/images/b/b6/Jmillenniumtowerburger.png/revision/latest?cb=20190730123312'
) ON CONFLICT (id) DO NOTHING;

insert into public.burgers (id, title, price, description, img_src) values (
    '3',
	'Spicy Cowboy Bacon Burger',
	'8.89',
	'Hamburger patties made with ground beef, jalapeno, and diced bacon, grilled and stacked with spicy cheese, barbecue sauce, and fried onions! [<a href="https://www.aspicyperspective.com/spicy-cowboy-bacon-burgers/">src</a>]',
	'https://www.aspicyperspective.com/wp-content/uploads/2019/05/spicy-cowboy-bacon-burgers-recipe-31-256x256.jpg'
) ON CONFLICT (id) DO NOTHING;

insert into public.burgers (id, title, price, description, img_src) values (
    '4',
	'Juicy Grilled Burger',
	'14.51',
	'Divide burger meat into 4 equal, 5-ounce portions. Gently press each round (without overworking the meat) into a patty ½-inch wider than the bun and about ½-inch thick (depending on the size of the bun). Gently turn the patty around while slightly pressing an indentation around the edge (kind of like pressing a moat inside the burger’s edge). [<a href="https://www.thecookierookie.com/juicy-grilled-burgers-recipe-how-to-grill-burgers/?__cf_chl_jschl_tk__=pmd_ZOLx3p8ozFSGws9Ss6ZHDuZ5bi_OglKUtcNWqQyjI3E-1632803994-0-gqNtZGzNAlCjcnBszQhl">src</a>]',
	'https://www.thecookierookie.com/wp-content/uploads/2019/06/grilled-burgers-recipe-how-to-grill-burgers-1-of-9-650x874.jpg'
) ON CONFLICT (id) DO NOTHING;

insert into public.burgers (id, title, price, description, img_src) values (
    '5',
	'Luger Burger',
	'9.52',
	'It takes a mighty burger for us not to order a steak at Peter Luger Steakhouse, and this is that burger. Served until 3:45pm daily, the Luger Burger features extra thick bacon (not an understatement) and more than a half pound of beef. [<a href="https://coolmaterial.com/feature/burger-week-the-burger-bucket-list-25-burgers-you-need-to-try/">src</a>] ',
	'https://coolmaterial.com/wp-content/uploads/2014/05/luger-burger.jpg'
) ON CONFLICT (id) DO NOTHING;

insert into public.burgers (id, title, price, description, img_src) values (
    '6',
	'Le Pigeon Burger',
	'4.89',
	'The grilled Le Pigeon Burger has a nice, smoky flavor that is enhanced with each topping. Gabriel Rucker layers it with Tillamook white cheddar, lettuce slaw, and pickled onions along with some ketchup, mayo, and mustard. The ciabatta bun is also tremendous. [<a href="https://coolmaterial.com/feature/burger-week-the-burger-bucket-list-25-burgers-you-need-to-try/">src</a>]',
	'https://coolmaterial.com/wp-content/uploads/2014/05/le-pigeon-burger.jpg'
) ON CONFLICT (id) DO NOTHING;


insert into public.burgers (id, title, price, description, img_src) values (
    '7',
	'Whiskey King Burger',
	'5.55',
	'Jose Garces serves up this $26 plate of insanity over at Village Whiskey in Philly. It’s topped with maple bourbon glazed cipollini, bleu cheese, bacon, and a bit of foie gras. Sweet, fatty, and decadent. [<a href="https://coolmaterial.com/feature/burger-week-the-burger-bucket-list-25-burgers-you-need-to-try/">src</a>]',
	'https://coolmaterial.com/wp-content/uploads/2014/05/the-whiskey-king-village-whiskey.jpg'
) ON CONFLICT (id) DO NOTHING;
