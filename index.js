const express = require('express');
const bodyParser = require('body-parser');
const { randomBytes } = require('crypto');
const cors = require('cors');
const dotenv = require('dotenv').config();
const { Pool, Client } = require('pg');

const app = express();
app.use(bodyParser.json());
app.use(cors());

const convertRowsToItems = (list) => {
  const newList = list.map(({ img_src: imgSrc, ...rest }) => ({
    imgSrc,
    ...rest,
  }));
  return newList;
};

var pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  max: 20,
});

app.get('/items', (req, res) => {
  console.log("GET /items")

  pool.connect(function(err, client, done) {
    if(err) {
      res.status(503).send('DB connection error');
      return console.error('DB connection error', err);
    }
    else{
    client.query("SELECT * from burgers ORDER BY date_added DESC", function(err, result) {
      // call `done()` to release the client back to the pool
      done();
  
      if(err) {
        return console.error('error running query', err);
      }
      
      let items = convertRowsToItems(result.rows);
      res.status(200).json(items);

    });
  }
});

});

app.get('/items/titles', (req, res) => {
  pool
    .query('SELECT title from burgers')
    .then((result) => {
      res.status(200).json(result.rows);
    })
    .catch((e) => console.error(e.stack));
});

const filterPrice = (value) => {
  let valueFiltered = parseFloat(value);
  return isNaN(valueFiltered) ? 0 : parseFloat(valueFiltered.toFixed(2));
};

app.post('/items/create', (req, res) => {
  const { title, price, description, imgSrc } = req.body.item;
  console.log(req.body.item); //DEBUG
  pool
    .query(
      "INSERT into burgers (title, price, description, img_src) values ('" +
        title +
        "', '" +
        filterPrice(price) +
        "', '" +
        description +
        "', '" +
        imgSrc +
        "') RETURNING id",
    )
    .then((result) => {
      console.log(result);
      res.status(200).json({ id: result.rows[0].id }); //status 200 due to https://developer.mozilla.org/ru/docs/Web/HTTP/Status
    })
    .catch((e) => console.error(e.stack));
});

app.put('/items/update', (req, res) => {
  const { id, title, price, description, imgSrc } = req.body.item;
  pool
    .query(
      "UPDATE burgers SET title='" +
        title +
        "', price='" +
        filterPrice(price) +
        "', description='" +
        description +
        "', img_src='" +
        imgSrc +
        "' WHERE id=" +
        id,
    )
    .then((result) => {
      console.log(result); //DEBUG
      res.status(201).json({ rowCount: result.rowCount }); // 201 due to https://developer.mozilla.org/ru/docs/Web/HTTP/Status
    })
    .catch((e) => console.error(e.stack));
});

app.delete('/items/delete', (req, res) => {
  console.log(req.body);
  const id = req.body.id;
  pool
    .query('DELETE FROM burgers WHERE id=' + id)
    .then((result) => {
      console.log(result); //DEBUG
      res.status(200).json({ rowCount: result.rowCount });
    })
    .catch((e) => console.error(e.stack));
});

app.listen(process.env.PORT, () => {
  console.log('Server listening on ' + process.env.PORT);
}); 
